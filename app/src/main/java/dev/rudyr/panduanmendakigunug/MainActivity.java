package dev.rudyr.panduanmendakigunug;

import android.drm.DrmStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import dev.rudyr.panduanmendakigunug.Fragment.FragmentCompas;
import dev.rudyr.panduanmendakigunug.Fragment.FragmentEquip;
import dev.rudyr.panduanmendakigunug.Fragment.FragmentInfo;
import dev.rudyr.panduanmendakigunug.Fragment.FragmentTips;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tablayout_id);
        viewPager = (ViewPager) findViewById(R.id.viewpager_id);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragment
        adapter.AddFragment(new FragmentInfo(), "Info");
        adapter.AddFragment(new FragmentEquip(), "Equip");
        adapter.AddFragment(new FragmentCompas(), "Compas");
        adapter.AddFragment(new FragmentTips(), "Tips & Trik");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


        tabLayout.getTabAt(0).setIcon(R.drawable.ic_infomountain);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_equipment);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_compas);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_tips);


        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setElevation(0);
    }

}
