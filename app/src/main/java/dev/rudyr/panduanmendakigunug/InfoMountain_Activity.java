package dev.rudyr.panduanmendakigunug;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import dev.rudyr.panduanmendakigunug.Fragment.InfoMountainFragment;
import dev.rudyr.panduanmendakigunug.Fragment.JalurMountainFragment;
import dev.rudyr.panduanmendakigunug.Fragment.WaktutempuhMountainFragment;


public class InfoMountain_Activity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private TextView name,desc,desclong;
    private ImageView photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_mountain_);

        tabLayout = (TabLayout) findViewById(R.id.infotablayout_id);
        viewPager = (ViewPager) findViewById(R.id.infoviewpager_id);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragment
        adapter.AddFragment(new InfoMountainFragment(), "Profil Gunung");
        adapter.AddFragment(new JalurMountainFragment(), "Jalur Pendakian");
        //adapter.AddFragment(new WaktutempuhMountainFragment(), "Waktu Tempuh");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);

    }
}
