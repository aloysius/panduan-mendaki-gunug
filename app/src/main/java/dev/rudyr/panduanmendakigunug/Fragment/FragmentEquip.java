package dev.rudyr.panduanmendakigunug.Fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dev.rudyr.panduanmendakigunug.R;
import dev.rudyr.panduanmendakigunug.listPerlengkapan;

public class FragmentEquip extends Fragment {

    View v;
    private List<listPerlengkapan> lsPerlengkapan;

    public FragmentEquip() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.equip_fragment,container,false);

        return v;
    }


}
