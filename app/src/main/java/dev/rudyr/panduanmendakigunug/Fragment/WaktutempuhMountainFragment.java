package dev.rudyr.panduanmendakigunug.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dev.rudyr.panduanmendakigunug.R;

public class WaktutempuhMountainFragment extends Fragment {

    View v;
    public WaktutempuhMountainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_waktutempuh_mountain, container, false);
        return v;
    }


}
