package dev.rudyr.panduanmendakigunug.Fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import dev.rudyr.panduanmendakigunug.Info;
import dev.rudyr.panduanmendakigunug.R;
import dev.rudyr.panduanmendakigunug.RecyclerViewAdapter;

import static android.content.ContentValues.TAG;

public class FragmentInfo extends Fragment {

    View v;
    private RecyclerView myrecyclerview;
    private List<Info> lsInfo;

    public FragmentInfo() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.info_fragment, container, false);
        myrecyclerview = (RecyclerView) v.findViewById(R.id.info_recyclerview);

        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(getContext(),lsInfo);
        myrecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecyclerview.setAdapter(recyclerViewAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lsInfo = new ArrayList<>();

        /*
        lsInfo.add(new Info(
                "Gunung Gede",
                "2.958 mdpl",
                "Taman Nasional Gunung Gede Pangrango adalah salah satu taman nasional yang terletak di Provinsi Jawa Barat. " +
                        "Ditetapkan pada tahun 1980, taman nasional ini merupakan salah satu yang tertua di Indonesia.",
                getResources().getString(R.string.jalur_mtgede),
                R.mipmap.ic_mtgede,
                R.drawable.mt_gede
        ));
        */

        lsInfo.add(new Info(
                "Gunung Gede",
                "2.958 mdpl",
                "Taman Nasional Gunung Gede Pangrango adalah salah satu taman nasional yang terletak di Provinsi Jawa Barat. " +
                        "Ditetapkan pada tahun 1980, taman nasional ini merupakan salah satu yang tertua di Indonesia.",
                "Jalur Cibodas \n" +
                        "Ada banyak jalur pendakian di gunung Gede yaitu Jalur Cibodas, Jalur Gunung Putri, dan Jalur Selabintana.\n\n" +
                        "Untuk mencapai jalur Cibodas dimulai dari Jakarata menuju Cimacan ditempuh selama 2 jam dengan bus jurusan Cipanas bisa juga dari Bandung menuju Cimacan dengan bus jurusan Bogor dengan waktu tempuh 2 jam 30 menit.\n\n" +
                        "Perjalanan dari Cimacan menuju Cibodas ditempuh selama 30 menit dengan colt.Di Cibodas mengurus perizinan terlebih dahulu di kantor TNGP sebaiknya pendaki yang akan naik mengurus perizinan jauh hari mengingat peraturan di Taman Nasional Gede – Pangrango sangat ketat.\n\n" +
                        "Izin pendakian minimal 3 hari sebelum pendakian sudah harus dikantongi karena jumlah pendakian di TNGP  dibatasi 500 orang, Jalur Cibodas 300 orang, Jalur Gunung Putri 200 orang dan Jalur Selabintana 100 orang.\n\n" +
                        "Pendakian dimulai dari Kantor TNGP menuju telaga biru  selama 35 menit dan diteruskan menuju Panyangcangan dengan waktu 15 menit di Panyangcangan kita bisa beristirahat, dari Pannyangcangan terdapat 2 jalur sebelah kiri jalur pendaki dan sebelah kanan jalur menuju air terjun Cibeureum.\n\n" +
                        "Kemudian perjalanan dilanjutkan menuju Rawa denok l selama 25 menit dan diteruskan menuju Rawa denok ll selama 40 menit melalui tangga batu terdapat bekas pos yang sudah rusak, kemudian perjalanan dilanjutkan menuju Batu kukus ll dapat ditempuh selama 5 menit dan dilanjutkan menuju pemandangan selama 10 menit.\n\n" +
                        "Dari Pemandangan menuju Camp air panas  5 menit perjalanan kemudian dilanjutkan menuju Kandang batu selama 10 menit dan diteruskan menuju Pos rusak l selama 10 menit, dari Pos rusak l menuju Kandang badak ditempuh selama 35 menit dan diteruskan menuju Pos rusak ll selama 45 menit.\n\n" +
                        "Dari Pos rusak menuju Tanjakan Rante ditempuh selama 10 menit dan diteruskan menuju Batas Vegetasi selama 30 menit Batas vegetasi menuju Puncak Gede ditempuh selama 10 menit, dari Puncak Gede bisa menuju Alun – alun Suryakencana kurang lebih 30 menit.",
                R.mipmap.ic_mtgede,
                R.drawable.mt_gede
        ));

        lsInfo.add(new Info(
                "Gunung Ciremai",
                "3.078 mdpl",
                "Gunung Ceremai adalah gunung berapi kerucut yang secara administratif termasuk dalam wilayah dua kabupaten, yakni Kabupaten Kuningan dan Kabupaten Majalengka, Provinsi Jawa Barat.",
                "Jalur Linggarjati\n" +
                        "Ada banyak Jalur pendakian Jalur Linggarjati (Desa Linggarjati,Kab.Cirebon, jalur resmi), Jalur Palutungan (Kec.Cigugur Kab.Cirebon), Jalur Apuy(Desa Argamukti, Kec.Argapura, Kab.Majalengka)Ada banyak Jalur pendakian Jalur Linggarjati (Desa Linggarjati,Kab.Cirebon, jalur resmi), Jalur Palutungan (Kec.Cigugur Kab.Cirebon), Jalur Apuy(Desa Argamukti, Kec.Argapura, Kab.Majalengka) \n\n" +
                        "Untuk mencapai jalur Linggarjati awalnya dari Terminal Cirebon menuju Cilimus dapat ditempuh selama 30 menit dengan bus kemudian dilanjutkan menuju Gedung Perjanjian ditempuh selama 15 menit dengan bus, Gedung Perjanjian merupakan tempat bersejarah dikenal sebagai gedung perjanjian Linggarjati \n\n" +
                        "Dari Gedung Perjanjian menuju  Linggarjati ditempuh selama  30 menit dengan jalan kaki, Di Linggarjati terdapat banyak Basecamp, namun yang paling popular adalah Basecamp Bpk Achmad. Pendakian dimulai dari Linggarjati menuju Block Cibunar selama 2 jam perjalanan. \n\n"+
                        "Perjalanan dilanjutkan menuju Pinus selama 40 menit memasuki hutan pinus dari Pinus dilanjutkan menuju Leuweung Datar selama 1 jam 50 menit dan diteruskan menuju Condang Amis selama 15 menit melalui hutan jati di Condang Amis bisa ngecamp dan terdapat mati air temporer. \n\n" +
                        "Kemudian perjalanan dilanjutkan menuju Kuburan Kuda selama 1 jam 5 menit dan dilanjutkan menuju Pangalap sekitar 1 jam 10 menit sebaiknya di Kuburan kuda beristirahat sepuasnya karena akan melewati tanjakan setan. \n\n" +
                        "Dari Pangalap dilanjutkan menuju Tanjakan Binbin 10 menit dan dilanjutkan menuju tanjakan Saeruni selama 35 menit perjalanan dilanjutkan menuju Bapa Tere selama 1 jam 35 menit dan dilanjutkan menuju Batu Lingga 55 menit di Batu Lingga terdapat batu besar dahulu tempat bertapa Sunan Gunung Jati. \n\n" +
                        "Perjalanan dilanjutkan menuju  Sangga Buana l selama 25 menit dan dilanjutkan menuju sangga Buana ll selama 30 menit di Sangga Buana ll terdapat mata air temporer dari tetesan air bebatuan , kemudian perjalanan dilanjutkan menuju Pangasinan selama 40 menit dari Pangasinan menuju Puncak Linggarjati ditempuh kurang lebih 50 menit. \n\n" +
                        "Jalur Apuy \n\n "+
                        "Untuk mencapai Jalur Apuy awalnya dari stasiun Cirebon menuju Adisujipto selama 10 menit dengan colt dan selanjutnya menuju Kedawung ditempuh selama 10 menit dengan colt dari Kedawung Perjalanan dilanjutkan menuju Rajagaluh ditempuh kurang lebih 55 menit dengan colt. \n\n" +
                        "Dari Rajagaluh perjalanan dilanjutkan menuju Cigasong (Majalengka) ditempuh selama 15  menit dengan colt dan dilanjutkan menuju Maja dengan bus selama 30 menit dari Maja menuju Arga Lingga dengan colt selama 55menit dan diteruskan menuju Arga Mukti (Apuy ) dengan colt selama 5 menit. \n\n"+
                        "Perjalanan dimulai dari Arga Mukti (Apuy) menuju Pos l (Block Arban) selama 2 jam 35 menit ditempuh melalui perkebunan penduduk hutan Areal Logging dan mengikuti Jalur Logging tersebut kearah kanan selama perjalanan terdapat banyak pondok dan sampailah pada Pos l sebelah jalur Logging. \n\n" +
                        "Perjalanan dari Pos l menuju Pos ll (Block Simpang Lima) dapat ditempuh selama 50 menit menyusuri jalur pendakian disebelah kiri (menuju keatas ) melewati perkebunan penduduk dengan medan cukup terjal. \n\n" +
                        "Perjalanan dari Pos ll menuju Pos lll (Block Tegal Wasawa)ditempuh selama  1 jam 25 menit melewati hutan tropis dan perjalanan dilanjutkan menuju Pos lV (Block Tegal Jamuju) kurang lebih 2 jam 10 menit melalui hutan tropis. \n\n" +
                        "Perjalanan dilanjutkan menuju Pos V (Sangiang Rangka) dapat ditempuh selama 10 menit melalui hutan tropis dan dilanjutkan menuju Simpang Jalur (Palutungan) kurang lebih 1 jam 5 menit melalui medan bebatuan, Perjalan di lanjutkan menuju Puncak Pangeran Talaga (Sama dengan Jalur Palutungan selama 25 menit) .\n\n",
                R.mipmap.ic_mtciremai,
                R.drawable.mt_ciremai
        ));

        lsInfo.add(new Info(
                "Gunung Papandayan",
                "2.665 mdpl",
                "Gunung Papandayan adalah gunung api strato yang terletak di Kabupaten Garut, Jawa Barat tepatnya di Kecamatan Cisurupan. " +
                        "Gunung dengan ketinggian 2665 meter di atas permukaan laut itu terletak sekitar 70 km sebelah tenggara Kota Bandung",
                "Pos satu atau basecamp. \n" +
                        "Pos pendakian 1 sering disebut basecamp. Basecamp adalah tempat adalah pos pertama yang ada di setiap gunung untuk memulai pendakian. Di basecamp kamu bisa mempersiapkan segala kebutuhan  untuk mendaki ke salah satu gunung di jawa barat ini. Kamu juga bisa beristirahat untuk mempersiapkan tenaga untuk pendakian esok harinya.\n" +
                        "\n" +
                        "Untuk mencapai basecamp kamu bisa menaiki ojek atau mobil pickup dengan biaya 10.000 sampai 30.000 saja. \n\n" +
                        "Jika kamu sudah mencapai basecamp, kamu bisa melihat batu besar dan mencium aroma kawah gunung papandayan. \n\n" +
                        "Setelah melewati basecamp, kamu akan melewati jalur ekstrim yaitu melewati jalur berbatu dan curam. \n\n" +
                        "Kamu harus melewati jalur ini agar sampai di pos 2.\n\n" +
                        "Pos dua atau pondok selada\n" +
                        "Jalur pendakian gunung papandayan yang kedua adalah pondok selada. Sesampainya disini anda akan dimintai dana sumbangan sukarela. Jalur untuk ke pondok selada sangatlah landai dan sedikit berlumpur. Di pos 2 kamu bisa mendirikan tenda untuk sekedar berisitirahat sebelum melanjutkan perjalanan ke salah satu gunung di jawa barat ini.\n\n" +
                        "\n\n" +
                        "Sambil mendirikan tenda, kamu juga disarankan untuk memasak makanan karena perjalanan untuk ke puncak gunung papandayan lumayan jauh.\n\n" +
                        "Di pos dua ini juga terdapat kebun edelweis dan hutan mati, jadi kalian yang mendaki bisa berfoto disana.\n\n" +
                        "Untuk mencapai puncak papandayan, anda bisa memilih untuk melewati hutan mati atau jalur yang memiliki trek dengan kemiringan 70 derajat. \n\n" +
                        "Bagi kamu yang belum profesional disarankan untuk memilih jalur melalui hutan mati. Hutan mati di gunung ini terbilang cukup mencekam dan menakutkan. \n\n" +
                        "Untuk pendaki yang melewati jalur ini disarankan tidak berkata kotor atau berbuat seronok.\n\n" +
                        "Puncak papandayan\n" +
                        "Setelah kamu melewati berapa jalur pendakian gunung papandayan, kamu akan mencapai puncak salah satu gunung di jawa barat ini. \n\n" +
                        "Kamu akan menjumpai pemandangan alam yang sangat indah dan menakjubkan yang membuat kamu betah berlama lama disana. \n\n" +
                        "For your information ya gengs, di gunung papandayan kamu juga bisa melihat matahari terbit.\n\n" +
                        "Bagi kalian yang ingin melihat matahari terbit di salah satu gunung di jawa barat ini, kalian harus datang pagi pagi sekali. Kamu juga bisa melihat bunga edelweis yang tumbuh disana. Puncak papandayan cocok banget buat kamu yang suka foto foto di alam. Berfoto di puncak papandayan juga sangat instagenic banget buat kamu yang suka update.\n",
                R.mipmap.ic_mtpapandayan,
                R.drawable.mt_papandayan
        ));

        lsInfo.add(new Info(
                "Gunung Merbabu",
                "3.145 mdpl",
                "Gunung Merbabu adalah gunung api yang bertipe Strato yang terletak secara geografis pada 7,5° LS dan 110,4° BT.\n" +
                        "Ketinggian: 3.145 m\n" +
                        "Ketinggian relatif: 2.432 m\n" +
                        "Letusan terakhir: 1797\n" +
                        "Letak: Semarang, Boyolali, Magelang (Jawa Tengah), Indonesia\n" +
                        "Provinsi: Jawa Tengah",
                "Jalur  Genting (Selo) \n" +
                        "Ada banyak jalur untuk mencapai puncak Merbabu diantaranya Jalur Genting(Selo), Jalur Dakan (Wekas) Jalur Tekelan (Kopeng).\n\n" +
                        "Untuk mencapai jalur Genting (Desa Genting Kec.Selo, Kabupaten Boyolali) dari Magelang menuju Muntilan dapat ditempuh selama 15 menit dengan bus, perjalanan dari Muntilan menuju Selo dapat ditempuh selama 1 jam dengan bus jurusan Boyolali, dilanjutkan menuju Dusun Genting dapat ditempuh selama 1 jam dengan berjalan kaki.\n\n" +
                        "Pendakian dimulai dari Dusun Genting menuju Pos l (Balong) dapat ditempuh selama 1 jam 30 menit, menlalui hutan pinus dengan medan cukup landau, di Pos l terdapat pelataran yang cukup luas dengan kapasitas 3 tenda, perjalanan dilanjutkan menuju Pos ll (Pentur)dapat ditempuh 1 jam melalui tanjakan.\n\n" +
                        "Perjalanan dilanjutkan dari Pos ll menuju Sabana l dapat ditempuh selama 1 jam melalui medan yang cukup menanjak, dan dilanjutkan menuju Sabana ll dengan medan yang cukup landai, dari Sabana ll perjalanan dilanjutkan menuju Puncak Kenteng Songo dapat ditempuh selama 1 jam 30 menit melalui tanjakan-tanjakan melewati sebuah memoriam.\n\n" +
                        "Selanjutnya ada sebuah persimpangan antara dua jalur Yaitu jalur kiri memutar sebelah kiri mengikuti punggungan sisi kiri Jalur Lurus memotong jalur (potong kompas). Kawasan Sabana ll menuju Puncak Kenteng Songo langganan kebakaran.\n\n" +
                        "Jalur Dakan (Kedakan/Wekas)\n" +
                        "Untuk mencapai Dakan awalnya dari Magelang menuju Wekas dapat ditempuh selama 50 menit, dengan bus jurusan Kopeng (Salatiga),Jika membawa kendaraan sendiri melanjutkan perjalanan menuju koramil dapat ditempuh selama 2 menitdari Wekas.\n\n" +
                        "Perjalanan selanjutnya dari koramil (belok kanan) menuju dusun Genikan selama 20 menit selanjutnya menuju dusun Kedakan selama 5 menit Jalur Koramil bisa dilalui untuk pendakian massal, Pendakian dimulai dari Dakan menuju makam l dapat ditempuh selama 15 menit melalui perkebunan penduduk.\n\n" +
                        "Perjalanan dari Makam l menuju Makam ll ditempuh selama 5 menit Makam ll berup a sebuah rumah di dekat makam terdapat bak air yang cukup jernih, dilanjutkan menuju Simpang l dapat ditempuh selama 5 menit dan dilanjutkan menuju Simpang ll (ambil jalur kiri) menuju Pos l dapat ditempuh selama 30 menit  melalui punggungan sisi kiri .\n \n" +
                        "Perjalanan dari Pos l menuju jalur Pertemuan (Jalur Genikan kiri ) Dakan dapat ditempuh 5 menit melalui punggungan sisi kanan dan dilanjutkan menuju Pertemuan Jalur Gedaman (kiri) – Dakan dapat ditempuh selama 10 menit .Perjalanan dilanjutkan menuju  Pos ll /Pipa bocor dapat ditempuh selama 2 jam 15 menit melalui jalur pipa air, di pipa bocor terdapat area yang luas untuk ngecamp, di pipa bocor terdapat 2 jalur jalur sebelah kiri jalur pendakian sedangkan sebelah kanan jalur menuju Air terjun/jalur pemitik daun Ki Urat.\n\n" +
                        "Perjalanan dari Pipa Bocor menuju Helipad dapat ditempuh selama 50 menit melalui tanah berpasir dan gersang Helipad pertemuan Jalur Tekelan (Kopeng), dari Helipad menuju Pos V (Gunung Kukusan mengikuti jalur kopeng) selama 20 menit dan dilanjutkan menuju Jembatan Setan selama 1 menit kemudian menuju Geger Sapi kurang lebih 15 menit.\n\n" +
                        "Perjalanan dilanjutkan menuju Batu Besar (Simpang Syarif-Kenteng Songo/pertemuan jalur Diwak Tekelan) perjalanan dilanjutkan menuju Puncak Syarif (Pregodalem)dengan waktu 10 menit atau bisa langung menuju Simpang Bukit selama 20 menit\n\n" +
                        "Kemudian perjalanan dilanjutkan menuju Jalur Traverse selama 15 menit dan dilanjutkan menuju Ondho Rante/Memoriam 2 menit Ondho rante menuju Puncak Dieng /Puncak pasar bubrah 2 menit perjalanan dilanjutkan menuju Puncak Kenteng Songo selama 5 menit.",
                R.mipmap.ic_mtmerbabu,
                R.drawable.mt_merbabu
        ));

        lsInfo.add(new Info(
                "Gunung Merapi",
                "2.930 mdpl",
                "Gunung Merapi adalah gunung berapi di bagian tengah Pulau Jawa dan merupakan salah satu gunung api teraktif di Indonesia.\n" +
                        "Ketinggian: 2.930 m\n" +
                        "Ketinggian relatif: 1.356 m\n" +
                        "Letusan terakhir: 10 Maret 2014\n" +
                        "Provinsi: Jawa Tengah\n",
                "Jalur Selo (Jalur Resmi)\n" +
                        "Ada banyak jalur pendakian di gunung Gede yaitu Jalur Cibodas, Jalur Gunung Putri, dan Jalur Selabintana.\n" +
                        "Untuk mencapai Jalur Selo berawal dari Magelang menuju Muntilan dapat ditempuh selama 15 menit dengan bus. Perjalanan dari Muntilan menuju Selo dapat ditempuh selama 1 jam dengan bus jurusan Boyolali.Perjalanan dari Selo menuju Basecamp Selo  (Plalangan) dapat ditempuh selama 30 menit.\n\n" +
                        "Pendakian dimulai dari Basecamp Selo menuju Pondok Pendaki dapat ditempuh selama 15 menit melalui jalan aspal di pondok pendaki terdapat pelataran yang luas untuk menampung para pendaki, perjalanan dilanjutkan menuju Pos l dapat ditempuh selama 35 menit melalui perkebunan penduduk.\n\n" +
                        "Perjalanan dari Pos l menuju Pos ll dapat ditempuh selama 1 jam melalui medan bebatuan cukup terjal, perjalanan dilanjutkan menuju Tugu dapat ditempuh selama 1 jam 15 menit, tugu setinggi 1,5 menter berada di punggunngan.\n\n" +
                        "Perjalanan dari Pos l menuju Pos ll dapat ditempuh selama 1 jam melalui medan bebatuan cukup terjal, perjalanan dilanjutkan menuju Tugu dapat ditempuh selama 1 jam 15 menit, tugu setinggi 1,5 menter berada di punggunngan. \n\n" +
                        "Dari Tugu berlanjut menuju Pasar Bubrah (Pasar Bubar/Pelawangan/Pasar Setan) dapat ditempuh selama 5 menit Pasar brubah merupakan batas vegetasi, perjalanan dilanjutkan menuju Puncak Garuda dapat ditempuh selama 1 jam melalui bebatuan yang mudah longsor, menurut informasi Puncak Garuda telah Hancur pasca letusan 2006.\n\n" +
                        "Jalur Kinahrejo (Pra letusan 2006)\n" +
                        "Untuk mencapai jalur kinahrejo berawal dari Yogyakarta menuju kinahrejo dapat ditempuh selama 1 jam dengan kendaraan colt angkutan umum jurusan kaliurang sesampainya Basecamp Kinahrejo (Mbah Marijan/R.Ng Surakso Hargo/Juru Kunci  Merapi) diwajibkan melapor dulu dan meminta izin (Jalur Kinahrejo bedasarkan data Pra-Letusan 2006).\n\n" +
                        "Pendakian dimulai dari Kinahrejo menuju jalur pertemuan jalur Bebeng dapat ditempuh selama 25 menit melalui gapura Kinahrejo melalui semak-semak dan dilanjutkan menuju  Shelter dapat ditempuh selama 5 menit.\n\n" +
                        "Perjalanan dari Shelter menuju Pos l (Srimanganti) dapat ditempuh selama 30 menit  di Pos l terdapat pelataran yang cukup luas, perjalanan dari Pos l menuju Pos ll (Rudal/Labuhan Merapi) dapat ditempuh selama 1 jam sesampainya di Pos ll tampak pondok dan penampungan air dari drum besi.\n\n" +
                        "Perjalanan dari Pos  ll menuju Pos lll dapat ditempuh selama 1 jam dan dilanjutkan menuju Pos lV melalui tanjakan berpasir selama 1 jam perjalanan dan dilanjutkan menuju Pos V (Rangka Besi ) dapat ditempuh selama 1 jam melalui tanjakan berpasir.\n\n" +
                        "Perjalanan dari Pos V menuju Kendit dapat ditempuh selama 10 menit melalui tanjakan cukup terjal di sisi sebelah kiri terdapat pelataran cukup luas untuk ngecamp, perjalanan dilanjutkan menuju Pemancar Seismograf dapat ditempuh selama 1 jam 5 menit melalui medan batuan labil yang cukup terjal.Perjalana dilanjutkan menuju Kawah Mati dapat ditempuh selama 15 menit melalui jalur yang penuh asap belerang, perjalanan dilanjutkan menuju Puncak Garuda dapat ditempuh selama 10 menit.",
                R.mipmap.ic_mtmerapi,
                R.drawable.mt_merapi));

        lsInfo.add(new Info(
                "Gunung Sumbing",
                "3.371 mdpl",
                "Gunung Sumbing adalah gunung api yang terdapat di Jawa Tengah, Indonesia., gunung Sumbing merupakan gunung tertinggi ketiga di Pulau Jawa setelah Gunung Semeru dan Gunung Slamet.\n" +
                        "Ketinggian: 3.371 m\n" +
                        "Ketinggian relatif: 2.577 m\n" +
                        "Letusan terakhir: 1730\n" +
                        "Letak: Jawa Tengah, Indonesia\n" +
                        "Provinsi: Jawa Tengah\n",
                "Jalur Cibodas\n" +
                        "Jalur Garung (Jalur baru).\n" +
                        "Ada Banyak jalur pendakian menuju puncak sumbing diantaranya, jalur Garung(Dsn.Garung Ds.Butuh Kec.Kalijajar Kab.Wonosobo),Jalur Kacepit(Desa PagerGunung Kec.Bulu Kab.Temanggung), Jalur Kaliangkrik.\n\n" +
                        "Untuk mencapai jalur Garung awalnya dari wonosobo menuju Gapura Garung dapat ditempuh selama 45 menit, dengan bus jurusan temanggung tepatnya 800 m sebelum kledug, dari Temanggung menuju Gapura Garung ditempuh selama 1 jam menggunakan bus,perjalanan dari Garung menuju Basecamp baru ditempuh 5 menit melalui jalan aspal.\n\n" +
                        "Dilanjutkan menuju Basecamp lama dapat ditempuh kurang lebih 20 menit dan dilanjutkan menuju Masjid Al Mansyur dapat ditemuh selama 5 menit pada tahun 1994 dibuka jalur baru dan pendaki dan penduduk menyebutnya dua jalur pendakian dan masih bisa dilewati sampai sekarang, jalur baru lebih cepat waktu tempuhnya dan banyak pendaki yang melewatinya.\n\n" +
                        "Perjalanan dimulai dari masjid al Mansyur menuju simpang sesat dapat ditempuh selama 5 menit melalui jalur sisi kanan jembatan memasuki perkebunan, di Simpang Sesat terdapat 2 jalur yaitu jalur lurus (Jalur Pendakian) dan jalur Kanan (Jalur Perkebunan), Perjalanan dilanjutkan menuju Batas Hutan ditempuh selama 1 jam 10 menit melalui punggungan perkebunan penduduk dengan medan sukup terjal.\n\n" +
                        "Perjalanan dari Batas Hutan menuju Pos l (Kedung) dapat ditempuh selama 45 menit melalui hutan kawasan Bosweisen, dan menyeberangi sungai Kedung selanjutnya jalan tanjakan sesampainya di Pos l pada tahun 2002 terdapat pondok penduduk kemudian perjalanan dilanjutkan menuju Pos ll (Gatakan) ditempuh selama 45 menit melalui hutan dan medan berpasir gersang.\n\n" +
                        "Dilanjutkan dari Pos ll menuju Pos lll (Kawasan Krendegan) dapat ditempuh selama 1 jam 15 menit dan dilanjutkan menuju Simpangan Jalur lama dan baru ditempuh 1 jam perjalanan dilanjutkan menuju Pestan ditempuh kurang lebih 5 menit melalui punggungan bervegetasi dan dilanjutkan menuju Pasar Watu selama 25 menit di Pasar Watu terdapat pelataran sempit dengan kapasitas 1 tenda (kecil).\n\n" +
                        "Perjalanan dari Pasar Watu menuju Watu Kotak ditempuh 30 menit perjalanan melalui medan bebatuan dan perdu, di Watu Kotak terdapat curukan batu yang cocok untuk ngecamp karena dapat berlimbung dari badai kemudian perjalanan dilanjutkan menuju Tanah Putih ditempuh selama 35 menit melalui punggungan berbatu, Perjalanan dilanjutkan menuju Puncak Buntuditempuh selama 10 menit melalui medan bebatuan, jika melihat kebawah tampak kawah sumbing jika cuaca cerah tampak gunung Sindoro menjulang tinggi,gunung merapi,merbabu,gunung slamet, gunung ciremai,lawu.\n\n" +
                        "Jalur Cepit (Kacepit)\n" +
                        "Untuk mencapai jalur Cepit awalnya dari Magelang menuju RS Parakan dapat ditempuh selama 1 jam dengan bus, perjalanan dari RS Parakan menuju dusun Cepit dapat ditempuh selama 45 menit dengan colt. Dirumah Kadus kita dapat melapor dan menginap.\n\n" +
                        "Perjalanan dari Dsn.Cepit menuju Pos l dapat ditempuh selama 2 jam melalui lading penduduk, hutan cemara, sesampainya di Pos l terdapat pelataran dengan kapasitas 1 tenda dan plang Pos l, perjalanan dilanjutkan menuju Pos ll dapat ditempuh kurang lebih 1 jam melalui hutan lebat.\n\n" +
                        "Sesampainya terdapat pelataran sempit untuk satu tenda, perjalanan dilanjutkan menuju Watu Kasur dapat ditempuh selama 4 jam melalui semak area bekas kebakaran sungai-sungai kering bebatuan, perjalanan dari Watu Kasur menuju Kawah ditempuh selama 2 jam 30 menit melalui tangga-tangga batu, medan bebatuan, Watu Lawang, perjalanan dari kawah menuju puncak Sumbing dapat ditempuh selama 30 menit melalui tanjakan super terjal.",
                R.mipmap.ic_mtsumbing,
                R.drawable.mt_sumbing
        ));

        lsInfo.add(new Info(
                "Gunung Slamet",
                "3.342 mdpl",
                "Gunung Slamet adalah sebuah gunung berapi kerucut yang terdapat di Pulau Jawa, Indonesia. Gunung Slamet terletak di antara 5 kabupaten, yaitu Kabupaten Brebes, Kabupaten Banyumas, Kabupaten Purbalingga, dan Pemalang, Jawa Tengah, Indonesia\n" +
                        "Ketinggian: 3.428 m\n" +
                        "Ketinggian relatif: 3.284 m\n" +
                        "Letak: Banyumas, Brebes, Tegal, Purbalingga, dan Pemalang, Jawa Tengah, Indonesia\n" +
                        "Provinsi: Jawa Tengah\n",
                "Jalur Cibodas\n" +
                        "Ada banyak jalur pendakian di gunung Gede yaitu Jalur Cibodas, Jalur Gunung Putri, dan Jalur Selabintana.\n" +
                        "■ Untuk mencapai jalur Cibodas dimulai dari Jakarata menuju Cimacan ditempuh selama 2 jam dengan bus jurusan Cipanas bisa juga dari Bandung menuju Cimacan dengan bus jurusan Bogor dengan waktu tempuh 2 jam 30 menit.\n\n" +
                        "■ Perjalanan dari Cimacan menuju Cibodas ditempuh selama 30 menit dengan colt.Di Cibodas mengurus perizinan terlebih dahulu di kantor TNGP sebaiknya pendaki yang akan naik mengurus perizinan jauh hari mengingat peraturan di Taman Nasional Gede – Pangrango sangat ketat.\n\n" +
                        "■ Izin pendakian minimal 3 hari sebelum pendakian sudah harus dikantongi karena jumlah pendakian di TNGP  dibatasi 500 orang, Jalur Cibodas 300 orang, Jalur Gunung Putri 200 orang dan Jalur Selabintana 100 orang.\n\n" +
                        "■ Pendakian dimulai dari Kantor TNGP menuju telaga biru  selama 35 menit dan diteruskan menuju Panyangcangan dengan waktu 15 menit di Panyangcangan kita bisa beristirahat, dari Pannyangcangan terdapat 2 jalur sebelah kiri jalur pendaki dan sebelah kanan jalur menuju air terjun Cibeureum.\n\n" +
                        "■ Kemudian perjalanan dilanjutkan menuju Rawa denok l selama 25 menit dan diteruskan menuju Rawa denok ll selama 40 menit melalui tangga batu terdapat bekas pos yang sudah rusak, kemudian perjalanan dilanjutkan menuju Batu kukus ll dapat ditempuh selama 5 menit dan dilanjutkan menuju pemandangan selama 10 menit.\n\n" +
                        "■ Dari Pemandangan menuju Camp air panas  5 menit perjalanan kemudian dilanjutkan menuju Kandang batu selama 10 menit dan diteruskan menuju Pos rusak l selama 10 menit, dari Pos rusak l menuju Kandang badak ditempuh selama 35 menit dan diteruskan menuju Pos rusak ll selama 45 menit.\n\n" +
                        "■ Dari Pos rusak menuju Tanjakan Rante ditempuh selama 10 menit dan diteruskan menuju Batas Vegetasi selama 30 menit Batas vegetasi menuju Puncak Gede ditempuh selama 10 menit, dari Puncak Gede bisa menuju Alun – alun Suryakencana kurang lebih 30 menit.\n\n",
                R.mipmap.ic_mtslamet,
                R.drawable.mt_slamet));

        lsInfo.add(new Info(
                "Gunung Lawu",
                "3.265mdpl",
                "Gunung Lawu terletak di Pulau Jawa, Indonesia, tepatnya di perbatasan Provinsi Jawa Tengah dan Jawa Timur. Gunung Lawu terletak di antara tiga kabupaten yaitu Kabupaten Karanganyar, Jawa Tengah, Kabupaten Ngawi, dan Kabupaten Magetan, Jawa Timur.\n" +
                        "Ketinggian relatif: 3.118 m\n" +
                        "Ketinggian: 3.265 m\n" +
                        "Letusan terakhir: 1885\n" +
                        "Puncak: 3.118 m (10.230 ft); Posisi ke-76 gunung tertinggi di dunia\n" +
                        "Provinsi: Jawa Tengah\n" +
                        "Letak: Kabupaten Karanganyar, Jawa Tengah dan Kabupaten Magetan, Kabupaten Ngawi Jawa Timur, Indonesia",
                "Jalur Cemoro Sewu\n" +
                        "Ada beberapa Jalur Pendakaian diantaranya Jalur Cemoro Kandang(Kec.Tawangmangu Jawa Tengah),Jalur Cemoro Sewu (Sarangan,Kec.Plaosan,Jawa Timur) Jalur Cetho Kec.Karanganyar.\n\n" +
                        "Untuk mencapai Jalur Cemoro Sewu bermula dari Solo menuju Karanganyar ditempuh selama 1 jam 30 menit dengan bus, perjalanan dari Tawangmangu menuju Cemoro Sewudapat ditempuh selama 50 menit dengan colt melewati Cemoro Kandang.\n\n" +
                        "Dapat ditempuh juga dari Madiun menuju Sarangan (Telaga Sarangan) dapat ditempuh selama 1 jam dengan bus, Perjalanan dari Sarangan menuju Cemoro Sewu dapat ditempuh selama 20 menit dengan colt jurusan Tawangmangu dap dapat langsung melapor disekretariat AGL.\n\n" +
                        "Pendakian dimulai dari Cemoro Sewu menuju Sanggar Pamlangan (hanya lewat) ditempuh selama 2 menit, perjalanan dilanjutkan menuju Pondok Kayu dapat ditempuh selama 38 menit, melalui setapat bebatuan dan dilanjutkan menuju Sendang Panguripan /Sumber wesanan ditempuh selama 10 menit melalui perkebunan penduduk.\n\n" +
                        "Perjalanan dilanjutkan menuju Pos l/Wesen –Wesen (2100 mdpl) ditempuh selama 10 menit melalui medan tangga di Pos l terdapat pondok yang cukup panjang dan sebainya tidak usah nge-camp ditempat itu karena angker, perjalanan dilanjutkan menuju Pos ll Watu Gedek/Watu Jago ditempuh selama 1 jam  melalui Hutan Pinus, hutan akasia .\n\n" +
                        "Dari Pos ll perjalanan dilanjutkan menuju Pos lll /Watu Gede yang dapat ditempuh selama 1 jam 30 menit  dan dilanjutkan menuju Pos lV /Watu Kapur dapat ditempuh selama 1 jam melalui tanjakan yang cukup labil di Pos lV terdapat rangka-rangka kayu untuk bivak.\n\n" +
                        "Perjalanan dilanjutkan menuju Sumur Jalatunda selama 28 menit sumur Jalatunda merupakan terowongan bawah tanah, perjalanan dilanjutkan menuju Pos V /Pos Jalatunda ditempuh selama 2 menit dan dilanjutkan menuju Pawon Sewu selama 5 menit dan dilanjutkan menuju Sendang Drajad/Persimpangan Hargo Dumilah dan Hargo Dalem selama 15 menit.\n\n" +
                        "Di Sendang Drajad terdapat 2 jalur Hargo Dumilah (kiri) Hargo Dalem (kanan),perjalanan menuju Hargo Dalem dapat ditempuh selama 15 menit melalui jalur setapak trend kekanan, Perjalanan dari Sendang Drajad menuju Pondok Pertapa dapat ditempuh selama 28 menit dan dilanjutkan menuju Puncak Hargo Dumilah(Puncak Lawu) dapat ditempuh selama 2 menit.\n\n" +
                        "Jalur Cemoro Kandang\n" +
                        "Untuk mencapai Jalur Cemoro Kandang bermula dari Solo menuju Karanganyar ditempuh selama 1 jam 30 menit dengan bus, perjalanan dari Tawangmangu menuju Cemoro Kandang dapat ditempuh selama 45 menit dengan bus.\n\n" +
                        "Dapat ditempuh juga dari Madiun menuju Sarangan (Telaga Sarangan) dapat ditempuh selama 1 jam dengan bus, Perjalanan dari Sarangan menuju Cemoro Kandang  dapat ditempuh selama 25 menit dengan colt jurusan Tawangmangu, sesampainya di Cemoro Kandang dapat melapor di Pos Penjagaan.\n\n" +
                        "Pendakian dimulai dari Cemoro Kandang menuju Pos l/Taman Sari Bawah ditempuh selama 1 jam melalui jalan setapak di Pos l terdapat pondok dapat digunakan untuk beristirahat dan dilanjutkan menuju Pos ll/Taman Sari atas/Persimpangan air panas ditempuh selama 45 menit.\n\n" +
                        "Di pos ll terdapat 2 jalur, jalur lurus jalur pendakian sedangakan jalur kanan jalur air panas untuk pergi ke air panas diperlukan waktu 20 menit, Perjalanan dilanjutkan menuju Pos lll/Penggik ditempuh selama 1 jam 45 menit selama perjalanan terdapat kawah-kawah kecil di Jurang Karang Gupito.\n\n" +
                        "Perjalanan dilanjutkan menuju Pos lV/Cokro Sengrenge/Cokro Suryo/Memoriam dapat ditempuh selama 1 jam 10 menit selama perjalanan melalui sendang panguripan, perjalanan dari Pos lV menuju Pos V ditempuh selama 50 menit untuk menuju Pucak Hargo Dumilah secara garis besar terdapat 3 jalur yaitu jalur kiri, jalur tengah dan jalur kanan.\n\n" +
                        "Perjalanan dari Pos V menuju Simpang l ditempuh selama 5 menit, di Simpang l terdapat 2 jalur yaitu Jalur Lurus (Masuk ke Jalur kiri)dan Jalur kanan (Masuk ke jalur tengah dan jalur kiri), Jalur kiri dari Simpang l menuju Puncak Hargo Dumilah dapat ditempuh selama 1 jam melalui jalur yang memutar jauh.\n\n" +
                        "Perjalanan dari Simpang l menuju Simpang ll dapat ditempuh selama 5 menit , awalnya melalui tanjakan 5 meter, selanjutnya melalui jalan menurun sesampainya di Simpang ll terdapat 2 jalur Jalur lurus(masuk ke jalur tengah) dan jalur kanan (masuk ke jalur kiri).\n\n" +
                        "Jalur tengah dari Simpang ll menuju Puncak Hargo Dumilah ditempuh selama 20 menit melalui jalur setapak yang lebar, Jalur Kiri dari Simpang ll menuju Punggungan ditempuh selama 3 menit menerobos semak-semak perdu di sisi sebelah kanan terdapat (Telaga Kuning), Perjalanan dari Punggungan menuju Hargo Dumilah dapat ditempuh selama 2 menit.",
                R.mipmap.ic_mtlawu,
                R.drawable.mt_lawu));

        lsInfo.add(new Info(
                "Gunung Semeru",
                "3.676 mdpl",
                "Gunung Semeru atau Gunung Meru adalah sebuah gunung berapi kerucut di Jawa Timur, Indonesia. Gunung Semeru merupakan gunung tertinggi di Pulau Jawa, dengan puncaknya Mahameru, 3.676 meter dari permukaan laut.\n" +
                        "Ketinggian: 3.676 m\n" +
                        "Ketinggian relatif: 3.676 m\n" +
                        "Letusan terakhir: 2015\n" +
                        "Puncak: 3.676 m (12.060 ft)\n" +
                        "Provinsi: Jawa Timur\n" +
                        "Letak: Kabupaten Malang dan Kabupaten Lumajang, Jawa Timur, Indonesia\n",
                "Jalur Ranu Pane (Jalur Resmi)\n" +
                        "Ada beberapa jalur untuk mendaki Gunung Semeru, antara lain Jalur Ranu Pane(Jalur Resmi), Jalur Ayak-ayak, dan Jalur Watu Pecah.\n" +
                        "Untuk mencapai Jalur Ranu Pane ditepuh dari Malang menuju Tumpang dapat ditempuh selama 30 menit dengan bus, perjalanan dari Tumpang menuju Gubugklakah dapat ditempuh selama 45 menit dengan jeep/truk jurusan Ranu Pane,di Gubugklakah ini dapat mengurus izin perizinan di Kantor Balai TNBTS Seksi Konservasi  Wilayah lll dengan alamat Jl. Raya Wringin Anom No.16Telp.(0341)787972 Poncokusumo Malang-Jawa Timur.\n\n" +
                        "Perjalanan selanjutnya dari Gubugklakah menuju Ranu Pane dapat ditempuh selama 1 jam 15 menit dengan jeep/truck,di Ranu Pane terdapat Information Center Kantor Resort,dan souvenir TNBTS, di kantor tersebut kita dapat mengurus perizinan pendakian.\n\n" +
                        "Pendakian dimulai dari Ranu Pane menuju Watu Rejeng dapat ditempuh selama 2 jam melalui medan yang relative landau, perjalanan diteruskan menuju Pertemuan Jalur-Ayak ayak dengan waktu tempuh 1 jam 55 menit dengan medan yang cukup landau danbanyak terowongan semak, Perjalanan dilanjutkan menuju Ranu Kumbolo(Pondok Pendaki/Prasasti Ranu Kumbolo) dapat ditempuh selama 5 menit-Di Ranu Kumbolo terdapat Pondok pendaki maupun camping-ground.\n\n" +
                        "Perjalanan selanjutnya menuju Tanjakan Cinta dapat ditempuh selama 10 menit melalui jalur “tanjakan cinta”menurut mitos jika berhasil melalui tanjakan tersebut tanpa berhenti maka perjalanan cintanya akan lancar.Perjalanan dari Tanjakan Cinta menuju Oro oro Ombo dapat ditempuh selama 5 menit  dengan medan menurun.\n\n" +
                        "Perjalanan dilanjutkan menuju Kawasan Hutan Cemoro Kandang dapat ditempuh selama 45 menit melalui padang rumput, hutan cemara dan dilanjutkan menuju Sabana Jambangan dapat ditempuh selama 1 jam melalui hutan cemara padang rumput,padang edelweiss.\n\n" +
                        "Perjalanan dari Sabana Jambangan menuju Kalimati dapat ditempuh selama 1 jam melalui padang rumput edelweiss, di Kalimati terdapat pondok pendaki,MCKdan sungai kering, jika kritis air kita dapat menuju sumber Mani dapat ditempuh selama 40 menit dengan menyusuri sungai kering sampai dengan pertemuan 2 sungai.\n\n" +
                        "Perjalanan dilanjutkan menuju Arcopodo/Recopodo dapat ditempuh selama 1 jam melalui hutan cemara yang terjal, di tempat ini terdapat area pelataran sebaiknya rencanakan berangkat ke puncak saat esok subuh (jam 2 pagi) sehingga dapat melihat sunrise di puncak.\n\n" +
                        "Perjalanan dari Arcopodo menuju Khelik (Jalur putus/Memoriam ditempuh selama 10 menit) menurut informasi tempat ini cukup angker perjalanan dilanjutkan menuju Cemoro Tunggal (Pelawangan) dapat ditempuh selama 10 menit melalui punggunan Longsor, cemoro tunggal merupakan batas vegetasi.Perjalan selanjutnya menuju Mahameru (Puncak Semeru) dapat ditempuh selama 2 jam melalui tanjakan berpasir.\n\n" +
                        "Jalur Ayak Ayak\n\n" +
                        "Jalur Ayak ayak adalah bukan jalur resmi jika melewati jalur ini harus “berhati-hati”. Perjalanan dari Malang menuju Tumpang dapat ditempuh selama 30 menit dengan bus, perjalanan dari Tumpang menuju Gubugklakah dapat ditempuh selama 45 menit dengan jeep/truk jurusan Ranu Pane.\n\n" +
                        "Dari Gubukklakah perjalanan dilanjutkan menuju Ngadas selama 2 jam 30 menit dengan jeep atau truk, perjalanan dilakukan dari Ngadas menuju Puncak Ayak-ayak selama 6 jam melalui hutan pinus dengan medan yang cukup terjal.\n\n" +
                        "Perjalanan dilanjutkan dari Puncak Ayak yak menuju Sabana Panggonan Cilik dapat ditempuh selama 1 jam 45 menit Sabana cilik merupakan lembah Gunung Ayak ayak, Perjalanan dilanjutkan menuju Ranu Kumbolo dapat ditempuh selama 15 menit, [Perjalanan selanjutnya sama dengan jalur Ranu Pane\n\n",
                R.mipmap.ic_mtsemeru,
                R.drawable.mt_semeru));


        lsInfo.add(new Info(
                "Gunung Bromo",
                "2.329 mdpl",
                "Gunung Bromo atau dalam bahasa Tengger dieja \"Brama\", adalah sebuah gunung berapi aktif di Jawa Timur, Indonesia.\n" +
                        "Ketinggian: 2.329 m\n" +
                        "Ketinggian relatif: 586 m\n" +
                        "Letusan terakhir: 2016\n" +
                        "Provinsi: Jawa Timur\n" +
                        "Gunung bromo konon katanya pada jaman dulu kala ketika kerajaan majapahit mengalami serangan dari berbagai daerah, masyarakat pribumi bingung untuk mencari tempat tinggal hingga pada akhirnya mereka terpisah menjadi 2 kelompok. " +
                        "Kelompok pertama pergi ke arah bromo, dan kelompok yang kedua pergi ke arah Bali.\n" +
                        "\n \n" +
                        "Kedua tempat ini memiliki kesamaan yaitu sama-sama menganut kepercayaan agama Hindu." +
                        "Masyarakat yang tinggal di gunung bromo disebut sebagai suku Tengger. Nama tengger berasal dari Legenda Roro Anteng dan juga Joko Seger yang diyakini oleh masyarakat sekitar sebagai asal-usul nama Tengger itu “Teng” akhiran nama Roro Anteng dan “Ger” akhiran nama dari Joko Seger.\n" +
                        "\n\n" +
                        "Gunung bromo sendiri dipercaya oleh masyarakat sekitar sebagai gunung suci. " +
                        "Mereka menyebutnya sebagai gunung dewa brahma. Orang Jawa kemudian menyebutnya sebagai gunung bromo.",
                "Perjalanan Menuju Gunung Bromo" +
                        "Perjalanan menuju gunung bromo melalui pintu barat dari arah kabupaten pasuruan yaitu masuk dari desa Tosari untuk dapat menuju ke pusat objek wisata (lautan pasir) terbilang berat karena medan yang harus di tempuh tidak dapat dilalui oleh kendaraan roda 4 biasa. " +
                        "Hal ini dikarenakan jalanannya menurun dari penanjakan ke arah lautan pasir sangat lah curam dan berbahaya, kecuali menggunakan mobil jip." +
                        "\n\n" +
                        "Mobil jip ini disediakan oleh pihak pengelola wisata gunung bromo. " +
                        "Akan tetapi banyak pula para wisatawan yang lebih memilih untuk berjalan kaki untuk dapat menuju lokasi wisata. " +
                        "Namun apabila kita dapat melalui pintu utara dari arah sebelum masuk ke kabupaten Probolinggo yaitu di daerah Tongas, kita akan menuju desa cemoro lawang sebelum turun menuju lautan pasir.\n" +
                        "\n\n" +
                        "Rute ini tidaklah terlalu sulit dikarenakan jalannya tidak securam ketika melewati rute Pasuruan. Sehingga sepeda motor pun dapat melaluinya." +
                        "Namun jika kamu ingin banyak berfoto dari puncak, maka sebaiknya untuk menggunakan jalur barat." +
                        "Namun jika kamu ingin berpetualang, maka sebaiknya carilah rute yang jarang dilewati oleh wisatawan." +
                        "Tapi dengan persiapan dan peralatan yang memadai yah pastinya, biar tidak tersesat.\n" +
                        "\n" +
                        "Berlibur menuju gunung bromo dapat dibilang sangat praktis jika kamu sangat menyukai traveller dan melalui jalur utara." +
                        "Kamu bisa melalukan kunjungan dalam jangka waktu 12 jam saja. Tentunya bila kamu memulainya dari Kota Surabaya, Malang, Jember, dan sekitarnya.\n" +
                        "\n" +
                        "Perjalanan dapat dimulai dari jam 12 malam sehingga kamu akan sampai sekitar pukul 2 -3 pagi." +
                        "Dimana kamu dapat beristirahat terlebih dahulu sebelum melihat sunrise di puncak bromo. Nikmatilah pemandangan dari atas sampai jam 9 pagi.\n" +
                        "\n",
                R.mipmap.ic_mtbromo,
                R.drawable.mt_bromo));

        lsInfo.add(new Info(
                "Gunung Argopuro",
                "3.088 mdpl",
                "Gunung Argapura adalah sebuah gunung berapi kompleks yang terdapat di Jawa Timur, Indonesia. Gunung Argapura mempunyai ketinggian setinggi 3.088 meter. Gunung Argapura merupakan bekas gunung berapi yang kini sudah tidak aktif lagi.\n" +
                        "Ketinggian relatif: 2.745 m\n" +
                        "Ketinggian: 3.088 m\n" +
                        "Puncak: 2.745 m (9.006 ft)\n" +
                        "Pegunungan: Iyang-Argopuro\n" +
                        "Provinsi: Jawa Timur\n" +
                        "Jalur Pendakian: Jalur Bremi dan Jalur Baderan\n" +
                        "Spot alam: Sabana, savana, danau,\n" +
                        "Flora: Edelweis, ilalang, pohon balsa\n" +
                        "Fauna: Kijang, Rusa, Babi Hutan, Ayam Hutan, Merak, Macan\n" +
                        "Jenis hutan: hutan Dipterokarp Bukit, hutan Dipterokarp Atas, hutan Montane, dan Hutan Ericaceous atau hutan gunung.\n" +
                        "Sumber air: Danau Taman Hidup\n" +
                        "Puncak: Puncak Rengganis, Puncak Arca, Puncak Argopoera\n" +
                        "Pemandangan dari puncak: G. Semeru, Raung, Arjuno, Welirang\n" +
                        "Mitos: persemedian Dewi Rengganis\n" +
                        "Kismis (Kisah Misteri): Pendaki dilarang berteriak karena ada kisah pendaki yang hilang karena dianggap mengganggu Dewi Rengganis\n",
                "Jalur pendakian gunung argopuro via Bremi terletak di Desa Bremi Kecamatan Krucil Kabupaten Probolinggo.\n" +
                        "Jalur pendakian Argopuro adalah jalur terpanjang di Jawa hal tersebut dikarenakan biasanya para pendaki memiliki start pendakian dan finish-nya berbeda jalur.\n" +
                        "Jika berangkat via Bremi maka pulang via Baderan, dan sebaliknya.\n" +
                        "TRANSPORTASI\n" +
                        "Dari Jakarta: (Bus) turun di terminal Probolinggo kemudian dilanjutkan naik bus Akas kecil jurusan ke Bremi.\n" +
                        "(kereta) Turun di Stasiun Malang, lanjut naik kereta dari Malang ke Stasiun Probolinggo atau bus menuju Terminal Probolinggo.\n" +
                        "Dari stasiun/terminal Probolinggo lanjut naik bus ke Bremi.\n" +
                        "Dari Surabaya: (Bus) sama seperti di atas. (kereta) turun di stasiun Probolinggo. Dan seterusnya sama.\n" +
                        "BASECAMP/PENDAFTARAN\n" +
                        "Basecamp khusus untuk pendakian gunung Argopuro bisa dilakukan di Polsek Bremi sekaligus lakukan pendaftaran terlebihdahulu.\n" +
                        "MEMULAI PENDAKIAN\n" +
                        "Polsek Krucil (Bremi) – Danau Taman Hidup (4jam)\n" +
                        "Perjalanan awal dari Polsek kita akan melewati jalan aspal perkampungan lalu masuk ke dalam hutan." +
                        "Kemudian kita akan bertemu dengan hutan yang ditumbuhi pohon Balsa yang sangat rapat. " +
                        "Selanjutnya kita memasuki kawasan semak belukar tinggi dan agak sedikit alam terbuka serta trek yang semakin licin. " +
                        "Sepanjang perjalanan menuju Danau Taman Hidup kita akan banyak melewati semak-semak yang dihinggapi pecet (lintah) jadi harus hati-hati.\n" +
                        "\n" +
                        "Danau Taman Hidup – Hutan Lumut (30menit)\n" +
                        "Dari Danau Taman Hidup perjalanan dilajutkan dengan jalan landai. Sepanjang perjalanan kita akan menyusuri aliran air yang mengihdupkan hutan di Argopuro ini.\n\n" +
                        "Hutan Lumut – Cemara Lima (2jam)\n" +
                        "Selanjutnya perjalanan mendaki bukit dan turun bukit sampai pada pos yang dipenuhi pohon cemara.\n\n" +
                        "Cemara Lima – Angkene (3jam)\n" +
                        "Perjalanan selanjutnya kita akan berhadapan dengan jalan yang dipenuhi semak ilalan setinggi 2 meter dengan jalan yang licin. " +
                        "Di jalur ini kita harus tetap hati-hati karena sewaktu-waktu bisa terpleset. " +
                        "Dan tetap waspada terhadap badan yang sewaktu-waktu juga bisa kena sabetan ilalang ataupun gigitan pacet.\n\n" +
                        "Angkene – Cisentor (2jam)\n" +
                        "Perjalanan menuju Pos Cisentor kita akan melewati sabana dan savana." +
                        "Pos Cisentor ditandai dengan bangunan petak ukuran 3 x 3 meter dan merupakan titik tengah antara jalur Bremi dan Baderan.\n\n" +
                        "Cisentor – Rawa Embik (2jam)\n" +
                        "Perjalanan selanjutnya masih sama seperti sebelumnya. Kita akan bertemu dengan trek yang sesak dengan ilalang dan trek licin.\n\n" +
                        "Rawa Embik – Puncak (1jam)\n" +
                        "Dari Rawa Embik menuju puncak kita akan melewati jalur yang penuh sesak dengan berbagai macam vegetasi dari mulai pohon cemara, ilalang, dan rerumputan. " +
                        "Di tengah perjalanan kita akan menjumpai percabangan yang disebut “Persimpangan Cinta” mitosnya di sini tempat pertemuan Dewi Rengganis dengan Rawa Embik. " +
                        "Perjalanan hanya satu jam saja untuk sampai pada puncak Rengganis.\n\n" +
                        "Puncak Arca dan Puncak Argopoera\n" +
                        "Di puncak antara puncak Rengganis, puncak Argopoera dan Puncak Arca letaknya saling berdekatan\n" +
                        "INFO PENTING: \n" +
                        "Perjalanan pendakian gunung Argopuro biasanya melalui satu jalur panjang dengan titik berangkat dan titik pulang berbeda.\n" +
                        "Jika mengambil perjalanan jalur Bremi biasanya pendaki memiliki jalur pulang ke Baderan. Dan sebaliknya, jika berangkat dari Baderan, maka ambil jalur pulang Bremi.\n" +
                        "\n" +
                        "kalkulasi perjalanan via Bremi,\n" +
                        "Polsek Krucil (Bremi) – Danau Taman Hidup (4jam)\n" +
                        "Danau Taman Hidup – Hutan Lumut (30menit)\n" +
                        "Hutan Lumut – Cemara Lima (2jam)\n" +
                        "Cemara Lima – Angkene (3jam)\n" +
                        "Angkene – Cisentor (2jam)\n" +
                        "Cisentor – Rawa Embik (2jam)\n" +
                        "Rawa Embik – Puncak (1jam)\n" +
                        "TOTAL = 14 jam 30 menit (belum dengan perjalanan turun).\n" +
                        "\n",
                R.mipmap.ic_mtargopuro,
                R.drawable.mt_argopuro));

        lsInfo.add(new Info(
                "Gunung Raung",
                "3.332 mdpl",
                "Gunung Raung adalah gunung berapi kerucut yang terletak di ujung timur Pulau Jawa, Indonesia. Secara administratif, kawasan gunung ini termasuk dalam wilayah tiga kabupaten di wilayah Besuki, Jawa Timur, yaitu Banyuwangi, Bondowoso, dan Jember.\n" +
                        "Ketinggian relatif: 3.069 m\n" +
                        "Ketinggian: 3.332 m\n" +
                        "Puncak: 3.069 m (10.069 ft); Ranked 83rd\n" +
                        "Provinsi: Jawa Timur\n" +
                        "Puncak: Puncak 17, Puncak Tusuk Gigi dan Puncak Sejati Raung\n" +
                        "Kondisi: perkebunan, hutan, kaldera, kawah, kabut\n" +
                        "Hutan: hutan Dipterokarp Bukit, hutan Dipterokarp Atas, hutan Montane, dan Hutan Ericaceous atau hutan gunung\n" +
                        "Flora: ilalang, pinus, cemara\n" +
                        "Fauna: berbagai macam burung\n" +
                        "Jalur pendakian:\n" +
                        "\n",
                "Jalur pendakian gunung Raung via Kalibaru \n" +
                        "merupakan jalur pendakian yang paling ekstrim dan susah. " +
                        "Jalur Kalibaru terletak di Banyuwangi, Jawa Timur. Untuk transport menuju Kalibaru cukup mudah. " +
                        "Jika kita naik kereta maka cukup turun di stasiun Kalibaru Banyuwangi. " +
                        "Kemudian menuju pos pendakian gunung Raung tempatnya di rumah Pak Soeto atau lebih tepatnya di Desa Wonokromo. " +
                        "Jika bertanya pada tukang ojek di stasiun Kalibaru biasanya mereka langsung tahu dan bisa mengantarkan menuju rumah Pak Soeto\n\n" +
                        "MEMULAI PENDAKIAN\n" +
                        "Basecamp Rumah Pak Soeto – Pos 1 Gareng\n" +
                        "Perjalanan menuju Pos 1 ada alternatif lain selain berjalan kaki yakni menggunakan jasa ojek (25rb/motor). " +
                        "Di Pos 1 terdapat sumber air dari sungai.\n\n" +
                        "Pos 1 – Pos 2 Semar (4jam)\n" +
                        "Perjalanan menuju Pos 2 memakan waktu yang lama dan melewati trek berupa hutan dan semak-semak belukar.\n" +
                        "Sampai di Pos 2 kita bisa memilih untuk mendirikan tenda dan bermalam. \n" +
                        "Di Pos 2 inilah tempat yang favorit atau cocok sekali untuk mendirikan tenda karena lahannya yang luas.\n\n" +
                        "Pos 2 – Camp 3 1656 Mdpl (1jam)\n" +
                        "Jalur selanjutnya menuju Camp 3 kita akan melewati jalur menanjak dan berpohon rindang.\n" +
                        "\n" +
                        "Camp 3 – Camp 4 1855 Mdpl (2jam)\n" +
                        "Jalur selanjutnya awalnya kita akan berjalan landai tapi kemudian kita akan melalui jalan yang lebih menanjak dari sebelumnya.\n" +
                        "\n" +
                        "Camp 4 – Camp 5 2115 Mdpl (40menit)\n" +
                        "Menuju Camp 5 tidak terlalu lama namun jalur yang dilewati lebih sulit.\n" +
                        "\n" +
                        "Camp 5 – Camp 6/Pos 3 Petruk 2285 Mdpl (30menit)\n" +
                        "Pada jalur menutu Pos 3 ini terbilang ekstrim karena kita akan melewati tiga pundak yang terjal.\n" +
                        "\n" +
                        "Camp 6 – Camp 7 2541 Mdpl (45 menit)\n" +
                        "Jalur selanjutnya semakin ekstim. Kita harus lebih berhati-hati dan tetap waspada.\n" +
                        "\n" +
                        "Camp 7 – Camp 8 2876 Mdpl (2jam)\n" +
                        "Pada jalur ini sebaiknya kita meninggalkan tas dan bawaan berat lain. Trek selanjutnya adalah trek climbing menggunakan peralatan seperti tali, carabiner dll.\n" +
                        "\n" +
                        "Camp 8 – Camp9/Pos 4 Bagong 3023 Mdpl (1jam)\n" +
                        "Camp 9 adalah batas menuju alam terbuka.\n" +
                        "\n" +
                        "Camp 9 – Puncak Bendera/ Puncak Kalibaru 3154 Mdpl (10menit)\n" +
                        "Sampailah pada puncak jalur Kalibaru. Mulai dari sini barulah kita akan menghadapi jalan paling ekstrim se-Indonesia. " +
                        "Yakni jalur menuju puncak 17, Puncak Tusuk Gigi dan Puncak Sejati gunung Raung.\n\n" +
                        "Untuk menuju puncak berikut perangkat dan apa-apa yang wajib yang harus dipenuhi:\n" +
                        "1. Persiapan Alat : Tali Kern 30m, Carrabiner, Webbing, Harnezt, Ascender, Helm, Jumar, Tali prusik. Semua harus dalam keadaan baik.\n" +
                        "2. Skill Teknis : Anchoring, Ascending, Belaying, descending Rappeling, Moving together. Minimal dalam team harus ada yang menguasai sehingga bisa jadi leader buat teman-temannya.\n" +
                        "3. Motivasi Team : doa, keselamatan adalah utama, saling dukung dan saling melengkapi.\n" +
                        "4. Logistik Team : Makanan, air minum, alat tambahan: kamera, GPS.\n\n" +
                        "Trek menuju puncak adalah jalan yang sangat curam dengan kanan-kiri jurang. Untuk itu kehati-hati-an adalah harga mati dan tentunya jangan lupa selalu berdoa.\n\n" +
                        "Puncak Bendera – Puncak17\n" +
                        "Perjalanan dimulai turun dari puncak bendera melipir igir-igir jurang berjalan satu-persatu dan bergantian. \n" +
                        "Di titik ini kita harus melipir tebing bebatuan dimana di sebelah kanan adalah jurang sedalam 50 meter, kita memasang jalur pemanjatan kurang lebih 5 meter, di jalur telah terpasang 1 buah hanger, 1 bolt dan di titik anchor atasnya terdapat pasak besi yang telah tertanam, dapat digunakan sebagai anchor utama.\n\n" +
                        "Leader melakukan artificial climbing sambil memasang jalur pemanjatan.\n" +
                        "Dapat menggunakan tali kern ataupun cukup membentangkan webbing.\n" +
                        "Setiap pendaki wajib memasangkan carabinernya jika melewati titik ini dan harus bergantian.\n\n" +
                        "Setelah melewati titik rawan 1 kita menuju puncak 17 / piramida, sampai pada titik rawan yang ke2 yaitu dibawah puncak 17.\n" +
                        "Disini kita kembali harus membuat jalur pemanjatan, dimana leader melakukan artificial climb selajutnya setibanya di puncak 17 memasang fix rope untuk dilalui orang selanjutnya dengan teknik jumaring.\n" +
                        "Atau pilihan lain adalah kita tidak kepuncak 17 tetapi melipir lewat samping puncak 17.\n" +
                        "Disini bisa menggunakan moving together jadi setiap anggota tim memasang carabinernya pada kern yang dibentangkan antara anggota paling depan dan paling belakang. \n" +
                        "Di titik ini juga terdapat beberapa anchor tanam yang bisa kita gunakan. Dibutuhkan fokus dan konsentrasi ekstra karena medan yang mudah rontok.\n\n" +
                        "Puncak 17 – Puncak Tusuk Gigi\n" +
                        "Tibalah kita di titik rawan yang ke3 / terakhir dimana kita harus memasang jalur untuk menuruni tebing sekurangnya 20 meter.\n" +
                        "Untuk itu menggunakan teknik rappelling untuk mencapai ke bawah. Dititik ini juga sudah ada beberapa anchor tanam dari besi yang dapat kita gunakan.\n" +
                        "Jalur Kern kita tinggal disini dan akan kita gunakan kembali nanti.\n" +
                        "\n" +
                        "Dibawah dilanjutkan dengan jalan yang agak menurun ke bawah sampai bertemunya jalur pungungan ke Puncak Tusuk Gigi ( dari jauh menyerupai tusuk gigi ).\n" +
                        "Dari situ kita akan disuguhi hamparan bebatuan yang semakin besar yang harus kita daki\n" +
                        "Dari tempat istirahat ini perjalanan kembali menanjak dengan tingkat kemiringan yang makin tegak.\n" +
                        "Waspadai juga longsor batuan lepas dari atas tidak membahayakan pendaki di bawahnya. Jalur bebatuan ini akan berakhir di puncak Tusuk Gigi dengan batuan sebesar rumah yang tersusun menjulang.\n" +
                        "\n" +
                        "Puncak Sejati Raung (8°07’32’’ LS dan 114°02’48 BT)\n" +
                        "Dari puncak Tusuk Gigi berorientasi ke kanan kita melipir ke belakang dan kemudian berjalan agak menanjak sekitar 100 meter tibalah kita di tempat yang menjadi tujuan akhir dari ekspedisi kita PUNCAK SEJATI GUNUNG RAUNG 3344 MDPL, ditandai dengan sebuah triangulasi dan pemandangan sebuah kawah besar yang masih aktif yang setiap saat mengeluarkan asapnya. \n" +
                        "Dari bawah kawah ini sering mengeluarkan suara dengan raungan yang menggelegar. Jika anda sampai di Puncak Sejati, suara ini akan lebih keras lagi, menggetarkan nyali dan sangat menakutkan.\n\n",
                R.mipmap.ic_mtraung,
                R.drawable.mt_raung));

    }

}
