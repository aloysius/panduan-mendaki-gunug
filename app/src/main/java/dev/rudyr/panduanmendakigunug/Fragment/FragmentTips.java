package dev.rudyr.panduanmendakigunug.Fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dev.rudyr.panduanmendakigunug.R;

public class FragmentTips extends Fragment {

    View v;
    public FragmentTips() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tips_fragment, container, false);
        return v;
    }
}
