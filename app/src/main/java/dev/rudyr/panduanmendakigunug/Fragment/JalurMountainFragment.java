package dev.rudyr.panduanmendakigunug.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import dev.rudyr.panduanmendakigunug.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class JalurMountainFragment extends Fragment {

    private TextView jalur;
    View v;

    public JalurMountainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_jalur_mountain, container, false);
        jalur = (TextView) v.findViewById(R.id.jalurid);
        String Jalur = getActivity().getIntent().getExtras().getString("Jalur");

        jalur.setText(Jalur);

        return v;
    }

}
