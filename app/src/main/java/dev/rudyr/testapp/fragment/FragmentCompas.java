package dev.rudyr.testapp.fragment;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import dev.rudyr.testapp.R;

public class FragmentCompas extends Fragment implements SensorEventListener {

    private float currentDegree = 0.0f;
    ImageView imgView;
    TextView angle;
    private SensorManager mSensorManager;
    private Sensor compass;

    View v;

    public FragmentCompas() {
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.compas_fragment, container, false);
        imgView = (ImageView) v.findViewById(R.id.imageView);
        angle = (TextView) v.findViewById(R.id.angle);
        this.mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
//        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        compass = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(compass != null){
            mSensorManager.registerListener((SensorEventListener) this, compass, SensorManager.SENSOR_DELAY_NORMAL);
        }
        return v;

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float degree = (float) Math.round(event.values[0]);
        this.angle.setText("Heading: " + Float.toString(degree) + " degrees");
        RotateAnimation ra = new RotateAnimation(this.currentDegree, -degree, 1, 0.5f, 1, 0.5f);
        ra.setDuration(210);
        ra.setFillAfter(true);
        this.imgView.startAnimation(ra);
        this.currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
