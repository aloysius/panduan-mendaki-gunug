package dev.rudyr.testapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import dev.rudyr.testapp.R;

public class InfoMountainFragment extends Fragment {

    private TextView name,desc,desclong;
    private ImageView photo;
    View v;

    public InfoMountainFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_info_mountain, container, false);

        name = (TextView) v.findViewById(R.id.title_infomountain);
        desc = (TextView) v.findViewById(R.id.desc_infomountain);
        desclong = (TextView) v.findViewById(R.id.desclong_infomountain);
        photo = (ImageView) v.findViewById(R.id.img_infomountain);

        String Name = getActivity().getIntent().getExtras().getString("Name");
        String Desc = getActivity().getIntent().getExtras().getString("Desc");
        String DescLong = getActivity().getIntent().getExtras().getString("DescLong");
        int Photo = getActivity().getIntent().getExtras().getInt("Image");

        name.setText(Name);
        desc.setText(Desc);
        desclong.setText(DescLong);
        photo.setImageResource(Photo);

        return v;
    }


}
