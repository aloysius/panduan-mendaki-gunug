package dev.rudyr.testapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dev.rudyr.testapp.R;

public class JalurMountainFragment extends Fragment {

    private TextView jalur;
    View v;

    public JalurMountainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_jalur_mountain, container, false);
        jalur = (TextView) v.findViewById(R.id.jalurid);
        String Jalur = getActivity().getIntent().getExtras().getString("Jalur");

        jalur.setText(Jalur);

        return v;
    }
}
