package dev.rudyr.testapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import dev.rudyr.testapp.R;
import dev.rudyr.testapp.listPerlengkapan;

public class FragmentEquip extends Fragment {

    View v;
    private List<listPerlengkapan> lsPerlengkapan;

    public FragmentEquip() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.equip_fragment,container,false);

        return v;
    }
}
