package dev.rudyr.testapp;

public class listPerlengkapan {

    private String title;
    private String desc;
    private int img;

    public listPerlengkapan(String title, String desc, int img) {
        this.title = title;
        this.desc = desc;
        this.img = img;
    }

    //Getter
    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public int getImg() {
        return img;
    }

    //Setter
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImg(int img) {
        this.img = img;
    }

}
