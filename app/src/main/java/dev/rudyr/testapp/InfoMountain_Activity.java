package dev.rudyr.testapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import dev.rudyr.testapp.fragment.InfoMountainFragment;
import dev.rudyr.testapp.fragment.JalurMountainFragment;

public class InfoMountain_Activity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_mountain);


        //Toolbar toolbar = (Toolbar) findViewById(R.id.infotoolbar);
        //setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.infotablayout_id);
        viewPager = (ViewPager) findViewById(R.id.infoviewpager_id);

        name = (TextView) findViewById(R.id.title_infomountain);
        String Name = getIntent().getExtras().getString("Name");

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragment
        adapter.AddFragment(new InfoMountainFragment(), "Profil Gunung");
        adapter.AddFragment(new JalurMountainFragment(), "Jalur Pendakian");
        //adapter.AddFragment(new WaktutempuhMountainFragment(), "Waktu Tempuh");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);
        getSupportActionBar().setTitle(Name);




    }
}
