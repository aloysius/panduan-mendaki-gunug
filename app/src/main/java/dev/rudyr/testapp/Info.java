package dev.rudyr.testapp;

public class Info {

    private String Name;
    private String Desc;
    private String DescLong;
    private String Jalur;
    private String Waktutempuh;
    private int Thumb;
    private int Img;

    public Info() {
    }

    public Info(String name, String desc, String descLong, String jalurinfo, int thumb, int img) {
        Name = name;
        Desc = desc;
        DescLong = descLong;
        Jalur = jalurinfo;
        Thumb = thumb;
        Img = img;
    }

    //Getter


    public String getName() {
        return Name;
    }

    public String getDesc() {
        return Desc;
    }

    public String getDescLong() {
        return DescLong;
    }

    public String getJalur() {
        return Jalur;
    }

    public String getWaktutempuh() {
        return Waktutempuh;
    }

    public int getThumb() {
        return Thumb;
    }

    public int getImg() {
        return Img;
    }

    //setter
    public void setName(String name) {
        Name = name;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public void setDescLong(String descLong) {
        DescLong = descLong;
    }

    public void setJalur(String jalur) {
        Jalur = jalur;
    }

    public void setWaktutempuh(String waktutempuh) {
        Waktutempuh = waktutempuh;
    }

    public void setThumb(int thumb) {
        Thumb = thumb;
    }

    public void setImg(int img) {
        Img = img;
    }
    
}
