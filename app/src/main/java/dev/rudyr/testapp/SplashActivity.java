package dev.rudyr.testapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class SplashActivity extends Activity{


    class C00911 extends Thread {
        C00911() {
        }

        public void run() {
            try {
                C00911.sleep(3000);
                SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), MainActivity.class));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.splash);
        new C00911().start();




    }
}
