package dev.rudyr.testapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import static android.content.ContentValues.TAG;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {


    Context mContext;
    List<Info> mData;

    public RecyclerViewAdapter(Context mContext, List<Info> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.info_item,parent,false);
        MyViewHolder vHolder = new MyViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.name.setText(mData.get(position).getName());
        holder.desc.setText(mData.get(position).getDesc());
        holder.thumb.setImageResource(mData.get(position).getThumb());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,InfoMountain_Activity.class);
                //Log.d(TAG, "onClick: "+mData.get(position).getJalur());
                //pass data to InfoMountain_activity
                intent.putExtra("Name", mData.get(position).getName());
                intent.putExtra("Desc", mData.get(position).getDesc());
                intent.putExtra("DescLong", mData.get(position).getDescLong());
                intent.putExtra("Thumb", mData.get(position).getThumb());
                intent.putExtra("Image", mData.get(position).getImg());
                intent.putExtra("Jalur", mData.get(position).getJalur());
                Log.d(TAG, "onClick: Get Data");
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView desc;
        private ImageView thumb;
        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name_mountain);
            desc = (TextView) itemView.findViewById(R.id.desc_mountain);
            thumb = (ImageView) itemView.findViewById(R.id.thumb);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.listitem_id);

        }
    }
}
